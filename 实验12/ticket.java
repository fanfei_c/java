package 实验12;

import java.util.ArrayList;
import java.util.Random;

public class ticket {
	private static ArrayList<String> tickList=new ArrayList<String>();
	
	public static void initTicket(){
		for (int i = 0; i < 200; i++) {
			tickList.add("票 " + i);
        }
	}
	public static String sellTicket(){
		if (tickList.size() > 0) {
            Random random = new Random();
            int index = random.nextInt(tickList.size());
            String ticket = tickList.remove(index);
            return ticket;
        } else {
            return "无票可售";
        }	
	}
	
	public static int getTicketNum() {
		return tickList.size();
	}
}
