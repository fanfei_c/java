package 第十章;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Demo extends JFrame{
	
	public Demo() {
		//设置布局管理器
		FlowLayout f=new FlowLayout();
		setLayout(f);
		//放置10个按钮
	    for(int i=1;i<=10;i++) {
	    	add(new JButton("按钮"+i));
	    }
	}
	public static void main(String[] args) {
		JFrame frame=new Demo();
		frame.setVisible(true);
		frame.setSize(300,300);
	}
}
