package 第十章;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Demo2 extends JFrame {
	// 成员变量位置
	private JLabel lname = new JLabel("用户名:");
	private JTextField txtName = new JTextField(20);
	private JButton showButton = new JButton("显示");
	private JLabel message = new JLabel();

	public Demo2() {
		setLayout(null);
		lname.setBounds(80, 100, 150, 70);
		add(lname);
		txtName.setBounds(260, 100, 200, 70);
		add(txtName);
		showButton.setBounds(180,280, 150, 70);
		add(showButton);
		message.setBounds(100, 350, 300, 70);
        add(message);
		showButton.addActionListener(new MyListener());
	}
    class MyListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==showButton)
			{
				message.setText("你输入的名:"+txtName.getText());
			}
//			System.out.println("按子下按钮了");
//			message.setText("你输入的名:"+txtName.getText());
		}
    }
	public static void main(String[] args) {
          JFrame frame=new Demo2();
          frame.setVisible(true);
          frame.setSize(700,600);
	}
}
