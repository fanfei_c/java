package 实验八;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class main1 extends JFrame {
	// 成员变量位置
	private JLabel lname1 = new JLabel("用户名:");
	private JTextField txtName1 = new JTextField(20);
	private JLabel lname2 = new JLabel("密    码:");
	private JTextField txtName2 = new JTextField(20);
	private JButton showButton1 = new JButton("登录");
	private JButton showButton2 = new JButton("退出");
	private JLabel message1 = new JLabel();
	private JLabel message2 = new JLabel();

	public main1() {
		setLayout(null);
		lname1.setBounds(50, 20, 50, 50);
		add(lname1);
		lname2.setBounds(50,80, 50, 50);
		add(lname2);
		txtName1.setBounds(100, 20, 180, 40);
		add(txtName1);
		txtName2.setBounds(100, 80, 180, 40);
		add(txtName2);
		showButton1.setBounds(75,150, 80, 50);
		add(showButton1);
		showButton2.setBounds(180,150, 80, 50);
		add(showButton2);
	}

	public static void main(String[] args) {
          JFrame frame=new main1();
          frame.setVisible(true);
          frame.setSize(350,300);
	}
}
