package 实验八_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import 实验八.main1;

public class main2 extends JFrame{

	// 成员变量位置
		private JLabel lname1 = new JLabel("用户名:");
		private JTextField txtName1 = new JTextField(20);
		private JLabel lname2 = new JLabel("密    码:");
		private JTextField txtName2 = new JTextField(20);
		private JButton showButton = new JButton("登录");
		private JButton closeButton = new JButton("退出");
		private JLabel message1 = new JLabel();
		private JLabel message2 = new JLabel();

		public main2() {
			setLayout(null);
			lname1.setBounds(50, 20, 50, 50);
			add(lname1);
			lname2.setBounds(50,80, 50, 50);
			add(lname2);
			txtName1.setBounds(100, 20, 180, 40);
			add(txtName1);
			txtName2.setBounds(100, 80, 180, 40);
			add(txtName2);
			showButton.setBounds(75,150, 80, 50);
			add(showButton);
			closeButton.setBounds(180,150, 80, 50);
			add(closeButton);
			message1.setBounds(150, 200, 80, 50);
			add(message1);
			showButton.addActionListener(new MyListener1());
			closeButton.addActionListener(new MyListener2());
		}
		
		public class main3 extends JFrame{
			private JLabel message = new JLabel("！欢迎使用本程序！");
			public main3() {
				setLayout(null);
				message.setBounds(70, 70, 200, 50);
				add(message);
			}
		}
		
		class MyListener1 implements ActionListener{
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==showButton) {
					if(txtName1.getText().equals("fanfei") && txtName2.getText().equals("123")) {
						message1.setText("登录成功");
						JFrame frame2=new main3();
						frame2.setVisible(true);
				        frame2.setSize(350,300);
					}	
					else {
						message1.setText("登录失败");
					}
				}
			}
		}

		class MyListener2 implements ActionListener{
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==closeButton) {
					dispose();
				}
			}
		}

		public static void main(String[] args) {
	          JFrame frame=new main2();
	          frame.setVisible(true);
	          frame.setSize(350,300);
		}

}
