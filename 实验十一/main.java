package main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import Dao.AdminDao;
import tools.JDBUtils;

public class main {

	public static void main(String[] args) {
		PreparedStatement stmt=null;
		ResultSet rs=null;
		Connection conn=null;
		try {
			conn=JDBUtils.getConnection();
			//AdminDao.log(rs, stmt, conn);//测试登录方法
			AdminDao.delete(stmt, conn);//测试删除方法
			//AdminDao.add(stmt, conn);//测试添加方法
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//释放空间
			//JDBUtils.release(rs, stmt, conn);
			JDBUtils.release(stmt, conn);
		}
	}

}
