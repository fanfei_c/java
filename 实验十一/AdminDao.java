package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

import tools.JDBUtils;

public class AdminDao {
	//查询并登录
	public static void log(ResultSet rs,PreparedStatement stmt,Connection conn) {
		System.out.println("请输入用户名和密码:");
		Scanner sc=new Scanner(System.in);
		String user=new String(sc.next());
		String password=new String(sc.next());
		String sql="select name,password from t_user where name=? and password=?";
		try {
			stmt=conn.prepareStatement(sql);
			stmt.setString(1, user);
			stmt.setString(2, password);
			rs=stmt.executeQuery();
			if (rs.next()) {
			    System.out.println("用户登录成功！");
			} else {
			    System.out.println("用户登录失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBUtils.release(rs, stmt, conn);
		}
	}
	
	//查询并删除
	public static void delete(PreparedStatement stmt,Connection conn) {
		System.out.println("请输入用户名:");
		Scanner sc=new Scanner(System.in);
		String user=new String(sc.next());
		String sql="delete from t_user where name= ?";
		try {
			stmt=conn.prepareStatement(sql);
			stmt.setString(1, user);
			int num =stmt.executeUpdate();
			if (num > 0) {
			    System.out.println("用户删除成功！");
			} else {
			    System.out.println("未找到该用户:"+user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBUtils.release(stmt, conn);
		}
	}
	
	//添加用户
	public static void add(PreparedStatement stmt,Connection conn) {
		System.out.println("请输入id,用户名及密码:");
		Scanner sc=new Scanner(System.in);
		int id=sc.nextInt();
		String user=new String(sc.next());
		String password=new String(sc.next());
		String sql="insert into t_user values(?,?,?)";
		try {
			stmt=conn.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.setString(2, user);
			stmt.setString(3, password);
			int num =stmt.executeUpdate();
			if (num > 0) {
			    System.out.println("用户添加成功！");
			} else {
			    System.out.println("添加失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBUtils.release(stmt, conn);
		}
	}
}
