// window.onload = function () {
//     var messagesDiv = document.getElementById('messages');
//     var ws = new WebSocket('ws://localhost:8080/myWs');
//
//     ws.onopen = function () {
//         console.log('Connected to WebSocket server.');
//     };
//
//     ws.onmessage = function (event) {
//         var messageElement = document.createElement('p');
//         messageElement.textContent = event.data;
//         messagesDiv.appendChild(messageElement);
//         console.log('1111');
//     };
//
//     ws.onerror = function (event) {
//         console.error('WebSocket error observed:', event);
//     };
//
//     ws.onclose = function (event) {
//         console.log('WebSocket is closed now.');
//     };
// };
window.onload = function () {
    var messagesDiv = document.getElementById('update');
    var ws = new WebSocket('ws://localhost:8080/myWs');

    ws.onopen = function () {
        console.log('Connected to WebSocket server.');
    };

    // 更新数据
    ws.onmessage = function (event) {
        messagesDiv.innerHTML=event.data;
        console.log('1111');
    };

    ws.onerror = function (event) {
        console.error('WebSocket error observed:', event);
    };

    ws.onclose = function (event) {
        console.log('WebSocket is closed now.');
    };
};