import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Verify {
	//通用static判空方法
	static boolean isEm(String s) {
		return s.isEmpty();
	}
	//通用static求字符串长度方法
	static int len(String s) {
		return s.length();
	}
	//通用static判断是否是数字
	static boolean isDg(String s) {
		for(int i=0;i<s.length();i++) {
			if(s.charAt(i)<'0' || s.charAt(i)>'9') {
				return false;
			}
		}
		return true;
	}
	
	//static验证用户名合法性
	//规定首位必须为字母，长度不超过10
	static boolean verifyAdName(String s) {
		if(s.charAt(0)<'0'||s.charAt(s.length()-1)>'9') {
			return false;
		}
		if(s.length()>10) {
			return false;
		}
		return true;
	}
	
	//static验证密码合法性
	//规定密码不能纯数字，且不允许有连续重复字符，长度大于8位
	static boolean verifyPW(String s) {
		if(isDg(s)==true || s.length()<8) {
			return false;
		}
		for(int i=0;i<s.length()-1;i++) {
			char tmp=s.charAt(i);
			if(tmp==s.charAt(i+1)) {
				return false;
			}
		}
		return true;
	}
	
	//static验证确认密码和密码是否相同
	static boolean verifyPW2(String s1,String s2) {
		return s1.equals(s2);
	}
	
	//static验证年龄是否合法
	static boolean verifyAge(int age) {
		if(age<0 || age >150) {
			return false;
		}
		else {
			return true;
		}
	}
	
	//static验证邮箱有效性
	static boolean verifyEmail(String s) {
		String RULE_EMAIL = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$";
        //正则表达式的模式 编译正则表达式
        Pattern p = Pattern.compile(RULE_EMAIL);
        //正则表达式的匹配器
        Matcher m = p.matcher(s);
        return m.matches();    
	}
	
	//static验证邮编有效性
	static boolean verifyEnum(String s) {
		String RULE_ENUM = "^\\d{6}$";
        //正则表达式的模式 编译正则表达式
        Pattern p = Pattern.compile(RULE_ENUM);
        //正则表达式的匹配器
        Matcher m = p.matcher(s);
        return m.matches();    
	}
	
	//static验证身份证号有效性
	static boolean verifyIdNum(String s) {
		String RULE_IdNum = "^[1-9]\\d{5}(18|19|20)\\d{2}(0[1-9]|10|11|12)(0[1-9]|[1-2]\\d|30|31)\\d{3}[0-9Xx]$";
        //正则表达式的模式 编译正则表达式
        Pattern p = Pattern.compile(RULE_IdNum);
        //正则表达式的匹配器
        Matcher m = p.matcher(s);
        return m.matches();    
	}
	
	//static验证出生日期有效性
	static boolean verifyBirthday(String s) {
		String RULE_BIRTH = "^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
        //正则表达式的模式 编译正则表达式
        Pattern p = Pattern.compile(RULE_BIRTH);
        //正则表达式的匹配器
        Matcher m = p.matcher(s);
        return m.matches();    
	}
	
	//static验证手机号有效性
	static boolean verifyTele(String s) {
		String RULE_Tele = "^1[0-9]{10}$";
        //正则表达式的模式 编译正则表达式
        Pattern p = Pattern.compile(RULE_Tele);
        //正则表达式的匹配器
        Matcher m = p.matcher(s);
        return m.matches();    
	}
	
}
