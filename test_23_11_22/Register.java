import java.util.Scanner;

public class Register {

	public static void main(String[] args) {		
		
		//创建验证类
//		Verify verify=new Verify();
		
		//判断用户名合法性
		System.out.println("请输入要注册的用户名");
		String s1="";
		Scanner scanner=new Scanner(System.in);
		s1=scanner.next();
		boolean flag1=true;
		while(flag1) {
			if(Verify.verifyAdName(s1)!=true) {
				System.out.println("输入不合法，请重新输入要注册的用户名");
				s1=scanner.next();
			}
			else {
				System.out.println("输入合法");
				flag1=false;
			}
		}
		
		//判断密码合法性
		System.out.println("请输入密码");
		String s2="";
		s2=scanner.next();
		boolean flag2=true;
		while(flag2) {
			if(Verify.verifyPW(s2)!=true) {
				System.out.println("输入不合法，请重新输入密码");
				s2=scanner.next();
			}
			else {
				System.out.println("输入合法");
				flag2=false;
			}
		}

		//确认密码和密码是否相同
		System.out.println("请再输一遍密码，验证输入是否相同");
		String s3="";
		s3=scanner.next();
		boolean flag3=true;
		while(flag3) {
			if(Verify.verifyPW2(s2, s3)!=true) {
				System.out.println("输入错误，请重新输入密码");
				s3=scanner.next();
			}
			else {
				System.out.println("输入正确");
				flag3=false;
			}
		}
		
		//判断年龄有效性
		System.out.println("请输入年龄");
		int age=0;
		age=scanner.nextInt();
		boolean flag4=true;
		while(flag4) {
			if(Verify.verifyAge(age)!=true) {
				System.out.println("输入不合法，请重新输入年龄");
				age=scanner.nextInt();
			}
			else {
				System.out.println("输入正确");
				flag4=false;
			}
		}
		
		//6.邮箱有效性
		System.out.println("请输入邮箱");
		String s4="";
		s4=scanner.next();
		boolean flag5=true;
		while(flag5) {
			if(Verify.verifyEmail(s4)!=true) {
				System.out.println("输入不合法，请重新输入邮箱");
				s4=scanner.next();
			}
			else {
				System.out.println("输入正确");
				flag5=false;
			}
		}
		
		//7.邮编有效性
		System.out.println("请输入邮编");
		String s5="";
		s5=scanner.next();
		boolean flag6=true;
		while(flag6) {
			if(Verify.verifyEnum(s5)!=true) {
				System.out.println("输入不合法，请重新输入邮编");
				s5=scanner.next();
			}
			else {
				System.out.println("输入正确");
				flag6=false;
			}
		}
		
		//8.身份证号有效性
		System.out.println("请输入身份证号");
		String s6="";
		s6=scanner.next();
		boolean flag7=true;
		while(flag7) {
			if(Verify.verifyIdNum(s6)!=true) {
				System.out.println("输入不合法，请重新输入身份证号");
				s6=scanner.next();
			}
			else {
				System.out.println("输入正确");
				flag7=false;
			}
		}
		
		//9.生日有效性
		System.out.println("请输入出生日期,要求格式为XXXX-XX-XX");
		String s7="";
		s7=scanner.next();
		boolean flag8=true;
		while(flag8) {
			if(Verify.verifyBirthday(s7)!=true) {
				System.out.println("输入不合法，请重新输入出生日期");
				s7=scanner.next();
			}
			else {
				System.out.println("输入正确");
				flag8=false;
			}
		}
		
		//10.手机号有效性
		System.out.println("请输入手机号码");
		String s8="";
		s8=scanner.next();
		boolean flag9=true;
		while(flag9) {
			if(Verify.verifyTele(s8)!=true) {
				System.out.println("输入不合法，请重新输入手机号码");
				s8=scanner.next();
			}
			else {
				System.out.println("输入正确");
				flag9=false;
			}
		}	

	}

}
