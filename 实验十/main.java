package 实验十;

import java.sql.*;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PreparedStatement stmt=null;
		ResultSet rs=null;
		Connection conn=null;
		System.out.println("请输入用户名和密码:");
		Scanner sc=new Scanner(System.in);
		String user=new String(sc.next());
		String password=new String(sc.next());

		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/usr";
			String username="root";
			String passwd="sql";
			conn=DriverManager.getConnection(url, username, passwd);
			String sql="select name,password from t_user where name=? and password=?";
			stmt=conn.prepareStatement(sql);
			stmt.setString(1, user);
			stmt.setString(2, password);
			rs=stmt.executeQuery();
			if (rs.next()) {
			    System.out.println("用户登录成功！");
			} else {
			    System.out.println("用户登录失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(stmt!=null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				stmt=null;
			}
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				conn=null;
			}
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) {
		            e.printStackTrace();
		        }
		    }
		}
	}

}
