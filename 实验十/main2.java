package 实验十_2;

import java.sql.*;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PreparedStatement stmt=null;
		Connection conn=null;
		System.out.println("请输入用户名:");
		Scanner sc=new Scanner(System.in);
		String user=new String(sc.next());

		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/usr";
			String username="root";
			String passwd="sql";
			conn=DriverManager.getConnection(url, username, passwd);
			String sql="delete from t_user where name= ?";
			stmt=conn.prepareStatement(sql);
			stmt.setString(1, user);
			int num =stmt.executeUpdate();
			if (num > 0) {
			    System.out.println("用户删除成功！");
			} else {
			    System.out.println("未找到该用户:"+user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(stmt!=null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				stmt=null;
			}
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				conn=null;
			}
		}
	}

}

