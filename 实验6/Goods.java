package 实验六;

public class Goods {
	private String num;
	private String name;
	private int amount;
	private double price;
	public Goods(String num,String name,int amount,double price) {
		this.num=num;
		this.name=name;
		this.amount=amount;
		this.price=price;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
