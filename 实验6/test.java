package 实验六;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class test {

	public static void main(String[] args) {
		Goods good1=new Goods("001","羽绒服",200,400.0);
		Goods good2=new Goods("002","围巾",50,10.0);
		Goods good3=new Goods("003","棉鞋",400,200.0);
		Goods good4=new Goods("004","面罩",40,20.0);
		Goods good5=new Goods("005","棉裤",500,80.0);
		ArrayList<Goods> wareHouse =new ArrayList<Goods>();
		wareHouse.add(good1);
		wareHouse.add(good2);
		wareHouse.add(good3);
		wareHouse.add(good4);
		wareHouse.add(good5);
		User u1=new User("fanfei");//创建用户1
		int n=0;
		do
		{
			System.out.println("******欢迎使用小凡购物网站******");
			System.out.println("\t1.浏览商品并加入购物车");
			System.out.println("\t2.修改购物车商品");
			System.out.println("\t3.删除购物车商品");
			System.out.println("\t0.退出系统");
			System.out.println("********************************");
			System.out.print("请选择：>");
			Scanner scanner=new Scanner(System.in);
			n=scanner.nextInt();
			switch (n) {
			case 1:
				u1.add(wareHouse);
				break;
			case 2:
				u1.alter(wareHouse);
				break;
			case 3:
				u1.delete(wareHouse);
				break;
			default:
				break;
			}
		}while(n!=0);
		
		System.out.println("系统已退出，欢迎下次使用！");
	}

}
