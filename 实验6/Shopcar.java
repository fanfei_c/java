package 实验六;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Shopcar{
	HashMap<String, Integer> shopCar=new HashMap<String, Integer>();
	public void display(ArrayList<Goods> wareHouse)
	{
		System.out.println("编号\t名称\t库存\t价格");
		Iterator it=wareHouse.iterator();
		while(it.hasNext())
		{
			Goods goods=(Goods)it.next();
			System.out.println(goods.getNum()+"\t"+goods.getName()+"\t"+goods.getAmount()+"\t"+goods.getPrice());
		}
	}
	public Goods getGoods(String key,ArrayList<Goods> wareHouse) {
		Iterator<Goods> it=wareHouse.iterator();
		
		while (it.hasNext()) {
			Goods goods=it.next();
			if(key.equals(goods.getNum())) {
				return goods;
			}	
		}
		return null;
	}
	
	public void displayShopCar(ArrayList<Goods> wareHouse)
	{
		Set<String> keySet=shopCar.keySet();
		Iterator<String> it=keySet.iterator();
		System.out.println("您购物车内的商品有：");
		System.out.println("编号\t名称\t库存\t价格\t购买数量\t");
		while(it.hasNext())
		{
				String keyString=it.next();
				Goods goods=getGoods(keyString,wareHouse);
		      	System.out.println(goods.getNum()+"\t"+goods.getName()+"\t"+goods.getAmount()+"\t"
				+goods.getPrice()+"\t"+shopCar.get(keyString));
		}
		System.out.println("---------------------------------------");
	}
	
	//加入购物车方法
	public void add(ArrayList<Goods> wareHouse)
	{
		display(wareHouse);
		System.out.print("请输入要购买的商品编号：");
		Scanner scanner=new Scanner(System.in);
		String pNo=new String();
		pNo=scanner.next();

		while(getGoods(pNo, wareHouse)==null)
		{
			System.out.println("输入错误，请重新输入要购买的商品编号：");
			pNo=scanner.next();
		}
		System.out.print("请输入要购买的数量：");
		int n=0;
		n=scanner.nextInt();
		while(getGoods(pNo, wareHouse).getAmount()<n)
		{
			System.out.print("输入错误或库存不足，请输入要购买的数量：");
			n=scanner.nextInt();
		}
		Integer amount=new Integer(n);
		shopCar.put(pNo, amount);
		displayShopCar(wareHouse);
	}
	
	//修改购买数量
	public void alter(ArrayList<Goods> wareHouse) 
	{
		displayShopCar(wareHouse);
		System.out.print("请输入要修改的购物车商品编号：");
		Scanner scanner=new Scanner(System.in);
		String pNo=new String();
		pNo=scanner.next();

		while(getGoods(pNo, wareHouse)==null)
		{
			System.out.println("输入错误，请重新输入要修改的购物车商品编号：");
			pNo=scanner.next();
		}
		System.out.print("请输入要修改的购物车商品数量：");
		int n=0;
		n=scanner.nextInt();
		while(getGoods(pNo, wareHouse).getAmount()<n)
		{
			System.out.print("输入错误或库存不足，请输入要修改的购物车商品数量：");
			n=scanner.nextInt();
		}
		Integer amount=new Integer(n);
		
		if(shopCar.replace(pNo, amount)!=null)
			System.out.println("修改成功");
		else 
			System.out.println("修改失败");
		
		displayShopCar(wareHouse);
	}
	
	public void delete(ArrayList<Goods> wareHouse)
	{
		displayShopCar(wareHouse);
		System.out.print("请输入要删除的购物车商品编号：");
		Scanner scanner=new Scanner(System.in);
		String pNo=new String();
		pNo=scanner.next();

		while(getGoods(pNo, wareHouse)==null)
		{
			System.out.println("输入错误，请重新输入要删除的购物车商品编号：");
			
			pNo=scanner.next();
		}
		
		if(shopCar.remove(pNo)!=null)
			System.out.println("删除成功");
		else 
			System.out.println("删除失败");
		displayShopCar(wareHouse);
	}
}
