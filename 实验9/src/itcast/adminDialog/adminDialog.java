/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itcast.adminDialog;

import itcast.domain.Fruit;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author FANFEI
 */
public class adminDialog extends itcast.view.JFrame{
    private itcast.service.service adminService=new itcast.service.service();
    public adminDialog(){
        super();
        queryFruitItem();
    }
    public void queryFruitItem(){
        String[] thead={"水果编号","水果名称","水果单价(元)","计价单位"};
        ArrayList<Fruit> dataList=itcast.Dao.AdminDao.queryAllFruits();
        String[][] tbody=list2Array(dataList);
        TableModel dataModel=new DefaultTableModel(tbody, thead);
        jTable.setModel(dataModel);
    }
    
    public String[][] list2Array(ArrayList<Fruit> list){
        String[][] tbody=new String[list.size()][4];
        for(int i=0;i<list.size();i++)
        {
            Fruit fruit=list.get(i);
            tbody[i][0]=fruit.getFruitNum();
            tbody[i][1]=fruit.getFruitName();
            tbody[i][2]=fruit.getFruitPrice()+"";
            tbody[i][3]=fruit.getFruitUnit();
        }
        return tbody;
    }
    public void addFruitItem(){
        String addNumber=jTextField_add1.getText();
        String addName=jTextField_add2.getText();
        String addPrice=jTextField_add3.getText();
        String addUnit=jTextField_add4.getText();
        //调用Dao层方法对数据库进行修改
        boolean addSuccess=adminService.addFruit(addNumber,addName,addPrice,addUnit);
        if(addSuccess){
            //刷新表格
            queryFruitItem();
            JOptionPane.showMessageDialog(this, "成功添加数据！");
        }
        else{
            //弹窗错误提示
            JOptionPane.showMessageDialog(this, "水果编号不能重复，请检查数据！");
        }
    }
    public void updateFruitItem(){
        //获取数据
        String updateNumber=jTextField_alter1.getText();
        String updateName=jTextField_alter2.getText();
        String updatePrice=jTextField_alter3.getText();
        String updateUnit=jTextField_alter4.getText();
        //调用Dao层方法对数据库进行修改
        boolean updateSuccess=adminService.updateFruit(updateNumber,updateName,updatePrice,updateUnit);
        if(updateSuccess){
            //刷新表格
            queryFruitItem();
            JOptionPane.showMessageDialog(this, "修改数据成功！");
        }
        else{
            //弹窗错误提示
            JOptionPane.showMessageDialog(this, "没有这个编号的水果，请检查数据！");
        }
    }
    public void delFruitItem(){
        //获取数据
        String delNumber=jTextField_delete.getText();
        //调用Dao层方法对数据库进行修改
        boolean delSuccess=adminService.delFruit(delNumber);
        if(delSuccess){
            //刷新表格
            queryFruitItem();
            JOptionPane.showMessageDialog(this, "删除数据成功！");
        }
        else{
            //弹窗错误提示
            JOptionPane.showMessageDialog(this, "没有这个编号的水果，请检查数据！");
        }
    }
}
