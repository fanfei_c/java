/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itcast.Dao;

/**
 *
 * @author FANFEI
 */
import itcast.domain.Fruit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

import itcast.tools.JDBUtils;
import java.util.ArrayList;

public class AdminDao {
	//获取所有数据
	public static ArrayList<Fruit> queryAllFruits() {
                Connection conn=null;
                PreparedStatement stmt=null;
                ResultSet rs=null;
                ArrayList<Fruit> list=new ArrayList<Fruit>();
                
		String sql="select * from fruitTable";
		try {
                    conn=itcast.tools.JDBUtils.getConnection();
                    stmt=conn.prepareStatement(sql);
                    rs=stmt.executeQuery();
                    while(rs.next())
                    {
                        Fruit fruit=new Fruit();
                        fruit.setFruitNum(rs.getString("水果编号"));
                        fruit.setFruitName(rs.getString("水果名称"));
                        fruit.setFruitPrice(rs.getFloat("水果单价"));
                        fruit.setFruitUnit(rs.getString("计价单位"));
                        list.add(fruit);
                    }
                    return list;
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBUtils.release(rs, stmt, conn);
		}
                return null;
	}
	
	//删除
	public static void delete(String number) {
                Connection conn=null;
                PreparedStatement stmt=null;
                ResultSet rs=null;
		String sql="delete from fruitTable where 水果编号 = ?";
		try {
                    conn=itcast.tools.JDBUtils.getConnection();
                    stmt=conn.prepareStatement(sql);
                    stmt.setString(1, number);
                    int num =stmt.executeUpdate();
                    if (num > 0) {
                        System.out.println("数据删除成功！");
                    }
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBUtils.release(stmt, conn);
		}
	}
	
	//添加
	public static void add(Fruit fruit) {
                Connection conn=null;
                PreparedStatement stmt=null;
                ResultSet rs=null;
		String sql="insert into fruittable values(?,?,?,?)";
		try {
                    conn=itcast.tools.JDBUtils.getConnection();
                    stmt=conn.prepareStatement(sql);
                    stmt.setString(1, fruit.getFruitNum());
                    stmt.setString(2, fruit.getFruitName());
                    stmt.setFloat(3, fruit.getFruitPrice());
                    stmt.setString(4, fruit.getFruitUnit());
                    int num =stmt.executeUpdate();
                    if (num > 0) {
                        System.out.println("数据添加成功！");
                    }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBUtils.release(stmt, conn);
		}
	}   
}
