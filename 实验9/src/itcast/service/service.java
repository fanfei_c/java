/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itcast.service;

import itcast.domain.Fruit;
import java.util.ArrayList;

/**
 *
 * @author FANFEI
 */
public class service {
    
    //查询服务
    public ArrayList<Fruit> queryFruit(){
        ArrayList<Fruit> data =itcast.Dao.AdminDao.queryAllFruits();
        return data;
    }
    
    //添加服务
    public boolean addFruit(String number,String name,String price,String unit){
        ArrayList<Fruit> data =itcast.Dao.AdminDao.queryAllFruits();
        //查重
        for(int i=0;i<data.size();i++)
        {
            Fruit tempFruit=data.get(i);
            if(number.equals(tempFruit.getFruitNum())){
                return false;
            }
        }
        //封装
        Fruit fruit=new Fruit(number,name,Float.parseFloat(price),unit);
        //如果不重复，将数据添加到数据库中
        itcast.Dao.AdminDao.add(fruit);
        return true;
    }
    
    //删除服务
    public boolean delFruit(String num){
        ArrayList<Fruit> data =itcast.Dao.AdminDao.queryAllFruits();
        //查询
        for(int i=0;i<data.size();i++)
        {
            Fruit tempFruit=data.get(i);
            if(num.equals(tempFruit.getFruitNum())){
                itcast.Dao.AdminDao.delete(num);
                return true;
            }
        }
        //未找到
        return false;
    }
    
    //修改服务
    public boolean updateFruit(String number,String name,String price,String unit){
        ArrayList<Fruit> data =itcast.Dao.AdminDao.queryAllFruits();
        //查询
        for(int i=0;i<data.size();i++)
        {
            Fruit tempFruit=data.get(i);
            
            //依据主键编号来匹配，匹配到就执行删除然后再添加
            if(number.equals(tempFruit.getFruitNum())){
                itcast.Dao.AdminDao.delete(number);
                //封装
                Fruit fruit=new Fruit(number,name,Float.parseFloat(price),unit);
                itcast.Dao.AdminDao.add(fruit);//添加方法中已有查重操作
                return true;
            }
        }
        //不存在相同的编号
        return false;
    }
}
