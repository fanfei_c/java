/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itcast.domain;

/**
 * 水果编号、水果名称、水果单价(元)、计价单位
 * @author FANFEI
 */

public class Fruit {
    private String fruitNum;
    private String fruitName;
    private Float fruitPrice;
    private String fruitUnit;
    
    public Fruit(){
    }
    public Fruit(String number, String name, float parseFloat, String unit) {
        this.fruitNum=number;
        this.fruitName=name;
        this.fruitPrice=parseFloat;
        this.fruitUnit=unit;
    }

    public Float getFruitPrice() {
        return fruitPrice;
    }

    public void setFruitPrice(Float fruitPrice) {
        this.fruitPrice = fruitPrice;
    }

    public String getFruitUnit() {
        return fruitUnit;
    }

    public void setFruitUnit(String fruitUnit) {
        this.fruitUnit = fruitUnit;
    }

    public String getFruitNum() {
        return fruitNum;
    }

    public void setFruitNum(String fruitNum) {
        this.fruitNum = fruitNum;
    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

}
